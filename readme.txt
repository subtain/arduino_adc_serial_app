This is an arduino project that implements oversampling of 10 bit adc to increase
resolution to 14 bits adc. Further, a state machine based protocol over serial bus
is designed and implemented to exchange data between arduino and a host windows system.